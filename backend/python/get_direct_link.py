###############################################################################################################################################################################
# The following code snippet shows how to get direct links to an uploaded video file, knowing its uri.
# Direct links to video files at different resolution can be used to download or play videos hosted on Vimeo through a third-party player.
###############################################################################################################################################################################
import vimeo

client = vimeo.VimeoClient(
  token='80d8160d2d857666da9b611db8f0b292', # VIMEO PREMIUM
  key='429acbfda2f4b7b6b0c4f5d48dd4d07bb622aedf', #VIMEO PREMIUM
  secret='vFx1NbgHv3bUSZEuxL9JrE+8jFPm6EGiIymIjsHQlXjdKKs9sN2XjMtWGeHItDflXXKIYsknYh+TnfQlMhuTeuP5KR7ROgU0sNRweEpMzoIW/NcHfQNTkHKLxe2NQtw5' #VIMEO PREMIUM
)

#known uri  
uri = "/videos/690597944"

response = client.get(uri).json()
# get the link to the uploaded file
print(response['link'])
# get direct links and the following metadata to different versions of a converted video file previously uploaded:
# quality: hd, sd, etc
# rendition: 1080p, 720p, 540p, etc
# format: video/mp4, etc. 
link_list = response['files']
[print("quality: " + l['quality'] + " rendition: " + l['rendition'] + " format: " + l['type'] + " \nlink: " + l['link']) 
for l in link_list]
