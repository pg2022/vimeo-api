###############################################################################################################################################################################
# The following code snippet shows how to get the name and the description of a video uploaded to Vimeo
# and illustrates how to edit the same metadata.
###############################################################################################################################################################################
import vimeo

client = vimeo.VimeoClient(
  token='80d8160d2d857666da9b611db8f0b292', # VIMEO PREMIUM
  key='429acbfda2f4b7b6b0c4f5d48dd4d07bb622aedf', #VIMEO PREMIUM
  secret='vFx1NbgHv3bUSZEuxL9JrE+8jFPm6EGiIymIjsHQlXjdKKs9sN2XjMtWGeHItDflXXKIYsknYh+TnfQlMhuTeuP5KR7ROgU0sNRweEpMzoIW/NcHfQNTkHKLxe2NQtw5' #VIMEO PREMIUM
)
  
## get link, title, and description of an uploaded video
uri = "/videos/693472768"

response = client.get(uri, params={"fields": "link, name, description"}).json() 
print("Video URL: " + response['link'])
print(''.join(["Video Title: ", response['name'] if (response['name']) else 'not present']))
print(''.join(["Video Description: ", response['description'] if (response['description']) else 'not present']))

## edit the title and description
client.patch(uri, data={
    'name': 'La piccola Kikki',
    'description': 'Kikki nei suoi primi giorni' 
})
print('The title and description of %s has been edited' %uri)
response = client.get(uri, params={"fields": "link, name, description"}).json() 
print("Your new video title is: " + response['name'])
print("Your new video description is: " + response['description'])


## set Vimeo privacy: adding password protection
""" client.patch(uri, data={
    'privacy':{
        'view': 'anybody'
    }
}) 
print('%s will not  require a password to be viewed' %uri)  """