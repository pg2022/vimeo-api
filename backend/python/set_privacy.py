###############################################################################################################################################################################
# The following code snippet shows how to edit the privacy level of a video uploaded to a platform.
# In this case we change the privacy level from 'private' to 'public', so that anybody could access the video.
###############################################################################################################################################################################

import vimeo

client = vimeo.VimeoClient(
  token='80d8160d2d857666da9b611db8f0b292', # VIMEO PREMIUM
  key='429acbfda2f4b7b6b0c4f5d48dd4d07bb622aedf', #VIMEO PREMIUM
  secret='vFx1NbgHv3bUSZEuxL9JrE+8jFPm6EGiIymIjsHQlXjdKKs9sN2XjMtWGeHItDflXXKIYsknYh+TnfQlMhuTeuP5KR7ROgU0sNRweEpMzoIW/NcHfQNTkHKLxe2NQtw5' #VIMEO PREMIUM
)

#known uri  
uri = "/videos/690597944" 

# get video privacy
response = client.get(uri, params={"fields": "link, name, description, privacy.view"}).json()
privacy_constraint = response['privacy']['view']
print("At the moment the video file is visible to %s" % privacy_constraint)

# set video privacy
if (privacy_constraint == 'nobody'):
    client.patch(uri, data={
        'privacy':{
            'view': 'anybody'
        }
    })

response = client.get(uri, params={"fields": "link, privacy.view"}).json()    
print("Now your video is accessible to %s." % response['privacy']['view'])

# set a password
client.patch(uri, data={
    'privacy': {'view': 'password'},
    'password': 'MyPassw@rd'
})
print('The video will now require a password to be viewed on Vimeo.')