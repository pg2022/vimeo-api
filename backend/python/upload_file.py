###############################################################################################################################################################################
# The following code snippet illustrates how to upload a video file to the Vimeo Platform using the Vimeo Python SDK.
# To be able to accomplish this task you may need to perform the following steps:
#   a) create an account on Vimeo if you don't have one yet
#   b) go to My Apps page and create an app (in the App Name field type the name of the app, a brief description in the App Description field and the URL in the App URL field)
#   c) install PyVimeo (pip install PyVimeo)
#   d) get the client secret, client id and generate a token (for detailed information please refer to the documentation https://developer.vimeo.com/api/guides/start)
###############################################################################################################################################################################
import vimeo

client = vimeo.VimeoClient(
  token='9c56f09c2ba2814bae3d07668aa427ad', # VIMEO PREMIUM
  key='429acbfda2f4b7b6b0c4f5d48dd4d07bb622aedf', #VIMEO PREMIUM
  secret='vFx1NbgHv3bUSZEuxL9JrE+8jFPm6EGiIymIjsHQlXjdKKs9sN2XjMtWGeHItDflXXKIYsknYh+TnfQlMhuTeuP5KR7ROgU0sNRweEpMzoIW/NcHfQNTkHKLxe2NQtw5' #VIMEO PREMIUM
)

# upload a video
file_name = '/Users/ppalla/Movies/video/grigia1.mp4'
uri = client.upload(file_name, data={
  'name': 'Grigia',
  'description': 'Grigia is cleaning itself.'
})

 # check the transcode status to find out whether the video is ready for the Vimeo players
response = client.get(uri + '?fields=transcode.status').json()

if response['transcode']['status'] == 'complete':
  print('Your video finished transcoding.')
elif response['transcode']['status'] == 'in_progress':
  print('Your video is still transcoding.')
else:
  print('Your video encountered an error during transcoding.')

  ## get the link to the uploaded video
response = client.get(uri + '?fields=link').json()
print("Your video link is: " + response['link'])