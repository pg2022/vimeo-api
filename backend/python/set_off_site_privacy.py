###############################################################################################################################################################################
# The following code snippet shows how to set the privacy level for embedded videos.
###############################################################################################################################################################################

import vimeo

client = vimeo.VimeoClient(
  token='80d8160d2d857666da9b611db8f0b292', # VIMEO PREMIUM
  key='429acbfda2f4b7b6b0c4f5d48dd4d07bb622aedf', #VIMEO PREMIUM
  secret='vFx1NbgHv3bUSZEuxL9JrE+8jFPm6EGiIymIjsHQlXjdKKs9sN2XjMtWGeHItDflXXKIYsknYh+TnfQlMhuTeuP5KR7ROgU0sNRweEpMzoIW/NcHfQNTkHKLxe2NQtw5' #VIMEO PREMIUM
)

#known uri  
uri = "/videos/690597944" 

# get video privacy
response = client.get(uri, params={"fields": "link, name, description, privacy.embed"}).json()
privacy_constraint = response['privacy']['embed']
print("At the moment the video file is visible to %s" % privacy_constraint)

# set video privacy
allowed_domain="/privacy/domains/mydomain.com"
client.put(uri + allowed_domain)
client.patch(uri, data={
        'privacy':{
            'embed': 'whitelist'
        }
    })
print("%s will now be embeddable on http://mydomain.com" % uri)


# show the entire white list
response = client.get(uri + '/privacy/domains').json()
print(response['data'])

# delete a domain
client.delete(uri + '/privacy/domains/domaintodelete.com')
response = client.get(uri + '/privacy/domains').json()
print(response['data'])
