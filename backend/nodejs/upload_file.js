/****************************************************************************************************************************************************************************  
 The following code snippet illustrates how to upload a video file to the Vimeo Platform using the Vimeo NodeJS SDK.
 To be able to accomplish this task you may need to perform the following steps:
   a) create an account on Vimeo if you don't have one yet
   b) go to My Apps page and create an app (in the App Name field type the name of the app, a brief description in the App Description field and the URL in the App URL field)
   c) Load Vimeo NodeJS 
   d) get the client secret, client id and generate a token (for detailed information please refer to the documentation https://developer.vimeo.com/api/guides/start) 
******************************************************************************************************************************************************************************/

var Vimeo = require('vimeo').Vimeo;
var client = new Vimeo("429acbfda2f4b7b6b0c4f5d48dd4d07bb622aedf", "vFx1NbgHv3bUSZEuxL9JrE+8jFPm6EGiIymIjsHQlXjdKKs9sN2XjMtWGeHItDflXXKIYsknYh+TnfQlMhuTeuP5KR7ROgU0sNRweEpMzoIW/NcHfQNTkHKLxe2NQtw5", "80d8160d2d857666da9b611db8f0b292");


var file_name = "/Users/ppalla/Movies/video/kikki2.mp4"

client.upload(
    file_name,
    function (uri) {
      console.log('File upload completed. Your Vimeo URI is:', uri);
    },
    function (bytes_uploaded, bytes_total) {
      var percentage = (bytes_uploaded / bytes_total * 100).toFixed(2)
      console.log(bytes_uploaded, bytes_total, percentage + '%')
    },
    function (error) {
      console.log('Failed because: ' + error)
    }
  )

 